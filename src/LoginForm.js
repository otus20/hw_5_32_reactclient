import React from 'react';
import './LoginForm.css'
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Row } from 'react-bootstrap';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const name = target.name;

    this.setState({
      [name]: target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    
    fetch('/api/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        login: this.state.login,
        password: this.state.password
      })
    })
    .then(response => response.json())
    .then(json => {
      alert('Login completed');
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {
    return (
      <Row className='LoginForm-center'>
        <Form className="col-md-3" onSubmit={this.handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Control name="login" placeholder="Enter login" onChange={this.handleChange} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword"> 
            <Form.Control name="password" type="password" placeholder="Enter password" onChange={this.handleChange} />
          </Form.Group>

          <Button className="btn btn-primary w-100" variant="primary" type="submit">
            Login
          </Button>
        </Form>
      </Row>
    );
  }
}

export default LoginForm;